<?php
/**
 * @file
 * When the Views module is enabled, this submodule will read all
 * views and list those that are enabled and return a UID. You can
 * then use those views to provide recipients for ManyMail.
 */

/**
 * Implements hook_manymail_recipients().
 */
function manymail_views_manymail_recipients() {
  $recipients['manymail_views_send_view_form'] = array(
    'callback' => 'manymail_views_get_view_addresses',
    'values' => array('views'),
  );

  return $recipients;
}

/**
 * Implements hook_views_api().
 */
function manymail_views_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'manymail_views') . '/includes',
  );
}

/**
 * Implements hook_permission().
 */
function manymail_views_permission() {
  $perm['manymail send views'] = array(
    'title' => t('Send e-mail to custom views'),
  );

  if (variable_get('manymail_views_allowed_views_override', 1)) {
    $perm['manymail override views'] = array(
      'title' => t('Override preset recipient views'),
    );
  }

  return $perm;
}

/**
 * Implements hook_forms().
 */
function manymail_views_forms($form_id, $args) {
  $forms['manymail_views_send_view_form']['callback'] = 'manymail_send_form';

  return $forms;
}

/**
 * Implements hook_menu_alter().
 */
function manymail_views_menu_alter(&$items) {
  $items['manymail']['access arguments'][0][] = 'manymail send views';
}

/**
 * Implements hook_menu().
 */
function manymail_views_menu() {
  $items = array();

  $items['manymail/view'] = array(
    'title' => 'Mail recipient views',
    'description' => 'Send an e-mail to one or more recipient views.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('manymail_views_send_view_form'),
    'access arguments' => array('manymail send views'),
    'file' => 'includes/manymail.forms.inc',
    'file path' => drupal_get_path('module', 'manymail'),
    'weight' => 5,
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/config/manymail/targets/views'] = array(
    'title' => 'Recipient views',
    'description' => 'Define recipients through a view.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('manymail_views_targets_views_form'),
    'access arguments' => array('manymail admin'),
    'file' => 'includes/manymail_views.admin.inc',
    'weight' => 2,
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * E-mail recipients retrieval function.
 *
 * Retrieves all e-mail recipients of (a) certain view(s).
 *
 * @param array|string $view
 *   A view name or array of view names.
 *
 * @return ManyMailRecipientList
 *   A list of all recipients for (a) certain view(s)
 */
function manymail_views_get_view_addresses($view) {
  if (!is_array($view)) {
    $view = array($view);
  }

  $uids = array();
  foreach ($view as $name) {
    foreach (views_get_view_result($name) as $result) {
      $uids[] = $result->uid;
    }
  }

  $list = new ManyMailRecipientList();
  $list->addUser($uids);

  return $list;
}

/**
 * Finds all views that return a UID and are enabled.
 *
 * These views can then be used as valid recipient views.
 *
 * @return array
 *   View objects that are valid for use as a recipient view
 */
function _manymail_views_valid_views() {
  $views = views_get_enabled_views();

  foreach ($views as $name => $view) {
    if (empty($view->display['default']->display_options['fields']) || !array_key_exists('uid', $view->display['default']->display_options['fields'])) {
      unset($views[$name]);
    }
  }

  return $views;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @ingroup forms
 */
function manymail_views_form_manymail_views_send_view_form_alter(&$form, &$form_state, $form_id) {
  $admin = user_access('manymail override views');
  $all_views = _manymail_views_valid_views();

  // Allow all views if admins can override config page settings.
  // Otherwise retrieve allowed views from the config page and intersect
  // with views that are valid at the moment this function is called.
  $views = (variable_get('manymail_views_allowed_views_override', 1) && $admin)
    ? $all_views
    : array_intersect_key($all_views, array_filter(variable_get('manymail_views_allowed_views', array())));

  if ($views) {
    foreach ($views as $name => $view) {
      $views[$name] = check_plain($view->human_name);
    }

    $form['recipient'] = array(
      '#type' => 'fieldset',
      '#title' => t('Recipients'),
      '#weight' => 1,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['recipient']['views'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Recipients',
      '#title_display' => 'invisible',
      '#options' => $views,
      '#description' => t('The recipient views to send this e-mail to.<br /><strong>Be careful what you select here: You may inadvertently e-mail a lot of people.</strong>'),
      '#required' => TRUE,
    );
  }
  else {
    $form = array(
      'error_no_views' => array(
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => t('There seem to be no recipient views configured for you to mail to.'),
      ),
    );
  }
}
