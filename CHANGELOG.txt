CHANGELOG for ManyMail for Drupal 7

ManyMail 7.x-1.0-beta8, 201?-??-??
==================================
Kristiaan Van den Eynde: Tiny code cleanup.
Kristiaan Van den Eynde: Fixed a display bug in 'show mail' page of mail logs.
Kristiaan Van den Eynde: Updated Mime hook execution weight.
#1850094 by Propaganistas, kristiaanvandeneynde: Fixed SQLite issues.
Kristiaan Van den Eynde: Enabled WYSIWYG support in ManyMail Mime.
Kristiaan Van den Eynde: Changed the way the signature field is hidden.
Kristiaan Van den Eynde: Removed admin-only info links on the front end.
Kristiaan Van den Eynde: Made tokens unclickable for disabled signature field.

ManyMail 7.x-1.0-beta7, 2012-11-06
==================================
Kristiaan Van den Eynde: Tagging for beta7 bug fix release.
Kristiaan Van den Eynde: Made a Lists description more clear.
Kristiaan Van den Eynde: Added case insensitive comparisons in Lists.
Kristiaan Van den Eynde: Fixed an incorrect duplicate recipient check.
Kristiaan Van den Eynde: Added version dependencies to the .info files.
Kristiaan Van den Eynde: Added configure links to submodules that require them.
#1559502 by kristiaanvandeneynde: Fixed don't show 'configuration page' link on form.

ManyMail 7.x-1.0-beta6, 2012-05-14
==================================
#1545576 by webadpro, kristiaanvandeneynde: Fixed noob error from beta5. :(
Kristiaan Van den Eynde: Changed redirect logic after sending or resuming an e-mail.
Kristiaan Van den Eynde: Added weights to the ManyMail page tabs.
Kristiaan Van den Eynde: Simplified some localization.
Kristiaan Van den Eynde: Updated an old doc link in manymail.module.
Kristiaan Van den Eynde: Fixed a bug where the SMTP check would cause a double redirect.
Kristiaan Van den Eynde: Fixed a php notice (unset variable).
Kristiaan Van den Eynde: Added functionality to resume an aborted e-mail.
Kristiaan Van den Eynde: Moved a submit handler for consistency.
Kristiaan Van den Eynde: Removed some form_stata abuse where form type value should be used.
Kristiaan Van den Eynde: Moved e-mail 'last activity' flagging before the SMTP check.
Kristiaan Van den Eynde: Removed watchdog logging of e-mails as a result of previous commit.
Kristiaan Van den Eynde: Added a better log overview at admin/config/manymail/logs.
Kristiaan Van den Eynde: Moved some code for readability.
Kristiaan Van den Eynde: Fixed a translatable string.
Kristiaan Van den Eynde: Code cleanup.

ManyMail 7.x-1.0-beta5, 2012-05-03
==================================
Kristiaan Van den Eynde: Tagging for beta 5 release.
#1558496 by kristiaanvandeneynde: Ajax error on form submission.
#1545576 by kristiaanvandeneynde: DB Schema not created.

ManyMail 7.x-1.0-beta4, 2012-04-12
==================================
Kristiaan Van den Eynde: Fixed a weird commit error, re-tagging for beta 4 release.
Kristiaan Van den Eynde: Tagging for beta 4 release.
Kristiaan Van den Eynde: Refactored code in manymail_lists_modify_form().
Kristiaan Van den Eynde: Use REQUEST_TIME instead of time() and track latest activity of a mail.
Kristiaan Van den Eynde: Fixed a critical bug where ManyMail would only mail half the intended people.
Kristiaan Van den Eynde: Converted some weight strings to weight integers, as they should be.
Kristiaan Van den Eynde: Added better per-recipient and per-mail tracking.
Kristiaan Van den Eynde: Fixed a bug in ManyMailRecipientList::attachToMailing that could cause duplicate key entry errors.
Kristiaan Van den Eynde: Refactored code within manymail_send_mail_batch() for performance and readability.
Kristiaan Van den Eynde: Some code readability improvements.
Kristiaan Van den Eynde: Added SMTP settings to static mail data so a batch keeps them from start to finish.
Kristiaan Van den Eynde: Added an SMTP connection check before sending e-mail and on viewing the settings.
Kristiaan Van den Eynde: Simplified ManyMail Lists admin overview screen.

ManyMail 7.x-1.0-beta3, 2012-03-20
==================================
Kristiaan Van den Eynde: Tagging for beta 3 release.
Kristiaan Van den Eynde: Added tracking of e-mail being successfully sent and an admin toggle for it.
Kristiaan Van den Eynde: Logging of e-mails, reworked batch set-up and preparation for 'resume mail' function.
Kristiaan Van den Eynde: Adjusted the database tables from the previous commit.
Kristiaan Van den Eynde: Added database tables for improved send performance and mail logging.
Kristiaan Van den Eynde: Completely reworked the ManyMail Lists admin interface to use a base form, paged table and delete interface.
Kristiaan Van den Eynde: Cleaned up some query code in ManyMail Lists.
Kristiaan Van den Eynde: Changed the database setup behind ManyMail Lists.
Kristiaan Van den Eynde: Fixed a copy-paste doxygen to actually show the correct information.
Kristiaan Van den Eynde: Replaced 'PHPMailer object' in manymail.api.php with 'ManyMailMailer object'.
Kristiaan Van den Eynde: Added an XMailer string to differentiate ManyMail from PHPMailer.
Kristiaan Van den Eynde: Corrected two localized strings.
Kristiaan Van den Eynde: Fixed permissions for send pages only checking for 'manymail send all' permission.
Kristiaan Van den Eynde: Fixed a bug where ManyMail Lists wouldn't save the admin override toggle.
Kristiaan Van den Eynde: Readded a Watchdog message that was deleted by accident in c519dd62676f.

ManyMail 7.x-1.0-beta2, 2012-03-07
==================================
Kristiaan Van den Eynde: Commented out attachment permission until that functionality exists. Beta 2 release.
Kristiaan Van den Eynde: Small change to the function signature of hook_manymail_mail_alter().
Kristiaan Van den Eynde: ManyMail MIME now sends correct XHTML and added hook_manymail_mime_mail_alter().
Kristiaan Van den Eynde: Whoops, manymail.dialog.css still contained a local dev path :)
Kristiaan Van den Eynde: Implemented hook_manymail_mail_meta() and hook_manymail_mail_alter() in MIME submodule.
Kristiaan Van den Eynde: Added administrative UI and form hooks to MIME submodule.
Kristiaan Van den Eynde: Initial commit for MIME submodule.
Kristiaan Van den Eynde: Fixed Lists submodule having @file doxygen of Views submodule.
Kristiaan Van den Eynde: Fixed a few code style issues.
Kristiaan Van den Eynde: Changed manymail_send_mail() to use strip_tags() if drupal_html_to_text() is not preferred.
Kristiaan Van den Eynde: Added hook_manymail_mail_meta().
Kristiaan Van den Eynde: Added a much prettier loading screen and renamed the html id batch-dialog to manymail-overlay.
Kristiaan Van den Eynde: Fixed the scrolltop behaviour to still work when a send form fails multiple times.
Kristiaan Van den Eynde: Added the choice to use drupal_thml_to_text() or not.
Kristiaan Van den Eynde: Moved all submodules into a separate folder.
Kristiaan Van den Eynde: Another day, another PAReview.
Kristiaan Van den Eynde: Fixed PHPMailer setting the Return-Path header twice.
Kristiaan Van den Eynde: Separated headers from content in admin interface and added Sender and Return-Path support.

ManyMail 7.x-1.0-beta1, 2012-02-22
==================================
Kristiaan Van den Eynde: Final code cleanup before first beta release.
Kristiaan Van den Eynde: Documented hook_manymail_recipients() and hook_manymail_recipients_alter().
Kristiaan Van den Eynde: Implemented hook_uninstall across the board.
Kristiaan Van den Eynde: Fixed a malformed description for signatures.
Kristiaan Van den Eynde: Changed all "allow admin override" toggles to work with permissions instead.
Kristiaan Van den Eynde: Better JavaScript code reuse and standards (and added misc/checkall.js).
Kristiaan Van den Eynde: Removed concatenation in localized strings.
Kristiaan Van den Eynde: Changed the entire form structure for sending mail to become extendable.
Kristiaan Van den Eynde: Added weight to the recipient configuration tabs
Kristiaan Van den Eynde: Added a character set option to the configuration.
Kristiaan Van den Eynde: When content type is text/plain, use drupal_html_to_text().
Kristiaan Van den Eynde: Added a very first hook: hook_manymail_mail_alter().
Kristiaan Van den Eynde: Fixed a few coding convention violations.
Kristiaan Van den Eynde: Made some more changes to the requirement checks
Kristiaan Van den Eynde: Updated punctuation in some watchdog messages.
Kristiaan Van den Eynde: Completely reworked manymail.install to no longer spam admin/reports/status and to be less restrictive.
Kristiaan Van den Eynde: Exit manymail_send_mail() with a Watchdog error if library could not be loaded.
Kristiaan Van den Eynde: Correctly use libraries_load() instead of own implementation.
Kristiaan Van den Eynde: Fixed a bug where mailing a List would throw a PDOException if it didn't contain any roles.
Kristiaan Van den Eynde: Fixed a whitespace error
Kristiaan Van den Eynde Fixed an incorrect occurrence of @see doxygen
Kristiaan Van den Eynde: Coding standards for ternary operators
Kristiaan Van den Eynde: Updated the helper message for reply-to addresses and made it consistent across front and back end
Kristiaan Van den Eynde: Changed the README to have clearer instructions.
Kristiaan Van den Eynde: Fixed some translation errors.


ManyMail 7.x-1.0-alpha5, 2012-02-02
===================================
Kristiaan Van den Eynde: Updated CHANGELOG.txt after alpha 5 release
Kristiaan Van den Eynde: Code cleanup in ManyMailRecipientList class
Kristiaan Van den Eynde: Removed an obsolete class from Lists submodule
Kristiaan Van den Eynde: Fixed a bug where _manymail_lists_valid_lists() would take blocked users into account
Kristiaan Van den Eynde: Implemented manymail_lists_get_list_addresses()
Kristiaan Van den Eynde: Made ManyMailRecipientList more compact through code reuse
Kristiaan Van den Eynde: Fixed ManyMailRecipientList:addData parameter hinting
Kristiaan Van den Eynde: Worked out manymail_lists_send_list_form()
Kristiaan Van den Eynde: Updated documentation to reflect the changes in 2d82d953fa and removed the distinct call (obsolete through union)
Kristiaan Van den Eynde: Another day, another PAReview.sh
Kristiaan Van den Eynde: Issue #1407886 by kristiaanvandeneynde: Disallow duplicates added through custom recipients
Kristiaan Van den Eynde: Fixed a bug in Lists submodule where the admin overview would incorrectly count all users
Kristiaan Van den Eynde: Added an edit and delete link + callback for Lists submodule admin panel
Kristiaan Van den Eynde: Fixed some documentation and punctuation
Kristiaan Van den Eynde: Changed check_plain usage to on-load, not on-save
Kristiaan Van den Eynde: Implemented _manymail_lists_valid_lists, WITHOUT check for valid views
Kristiaan Van den Eynde: Added a selection table in the admin menu for Lists submodule
Kristiaan Van den Eynde: Implemented validation and submission handlers for Lists list creation
Kristiaan Van den Eynde: Made Lists admin form names consistent with the database behind them
Kristiaan Van den Eynde: Updated hook_schema to specify lengths for all varchar (caused problems with MySQL)
Kristiaan Van den Eynde: Worked out the manymail_lists_list_exists() function
Kristiaan Van den Eynde: Removed a confusing comment
Kristiaan Van den Eynde: Initial, untested suggestion for Lists submodule hook_schema
Kristiaan Van den Eynde: Expanded Lists submodule admin form
Kristiaan Van den Eynde: Added skeleton for checking Lists machine names
Kristiaan Van den Eynde: Corrected a bug where the 'send to views' page wouldn't show tokens.
Kristiaan Van den Eynde: Added w.i.p. admin side of Lists submodule
Kristiaan Van den Eynde: Initial topical branch for lists module
Kristiaan Van den Eynde: Doxygen updates
Kristiaan Van den Eynde: Fixed a whitespace error before a require statement
Kristiaan Van den Eynde: Moved manymail.tokens.inc to module root so it can be found by module_implements
Kristiaan Van den Eynde: Fixed a missing parenthesis bug
Kristiaan Van den Eynde: Moved token hooks to separate file
Kristiaan Van den Eynde: Even more doxygen updates
Kristiaan Van den Eynde: Removed unnecessary Doxygen lines for hook_help implementation
Kristiaan Van den Eynde: Documented ManyMailRecipient members
Kristiaan Van den Eynde: Updated Doxygen to D8 coding standards for @param and @return types
Kristiaan Van den Eynde: Fixed a type casting bug in manymail_views_get_view_addresses
Kristiaan Van den Eynde: Corrected access argument for config/manymail/targets/views
Kristiaan Van den Eynde: Added hook_menu_alter to Views submodule
Kristiaan Van den Eynde: Fixed a file path issue for the manymail system admin page
Kristiaan Van den Eynde: More whitespace errors fixed
Kristiaan Van den Eynde: Fixed some whitespace errors
Kristiaan Van den Eynde: Correctly implemented class files
Kristiaan Van den Eynde: Corrected the permission required for admin page access
Kristiaan Van den Eynde: Removed some "funny" comments and added decent ones
Kristiaan Van den Eynde: Moved a lot of callbacks to inc files
Kristiaan Van den Eynde: Coding standards in INSTALL.txt
Kristiaan Van den Eynde: Removed .gitignore file
Kristiaan Van den Eynde: Renamed target roles to recipient roles to be in line with other target names
Kristiaan Van den Eynde: Updated manymail-recipient token to use a ManyMailRecipient
Kristiaan Van den Eynde: Renamed "recipient" token to "manymail-recipient" to avoid conflicts with other modules
Kristiaan Van den Eynde: Reworked Views submodule to no longer refer to Views as recipient groups
Kristiaan Van den Eynde: Corrected punctuation in submodule .info files
Kristiaan Van den Eynde: Coding standards...
Kristiaan Van den Eynde: Fixed some coding standards flaws
Kristiaan Van den Eynde: Updated module to use recipient classes ManyMailRecipient and ManyMailRecipientList
Kristiaan Van den Eynde: Fixed a bug where module defaults weren't taken into account for "admin override" flags
Kristiaan Van den Eynde: Updated the punctuation in some doxygen
Kristiaan Van den Eynde: Fixed a bug where token help would appear even though there were no text fields
Kristiaan Van den Eynde: More code cleanup after running PAReview.sh
Kristiaan Van den Eynde: Updated code to adhere to the Drupal coding standards
Kristiaan Van den Eynde: Replaced all line endings with UNIX type line endings
Kristiaan Van den Eynde: Added newline at end of some files
Kristiaan Van den Eynde: Removed license, will be added by packager
Kristiaan Van den Eynde: Renamed README to README.txt
Kristiaan Van den Eynde: Added all necessary files for a first release


ManyMail 7.x-1.0-alpha1, 2011-11-29
===================================
Kristiaan Van den Eynde: Updated SMTP config form usability
Kristiaan Van den Eynde: Changed log messages to be less branded
Kristiaan Van den Eynde: Fixed a bug where the SMTP host was incorrectly being set to the port
Kristiaan Van den Eynde: Added the SMTP configuration form and implementation
Kristiaan Van den Eynde: Altered the name of the send form's AJAX callback
Kristiaan Van den Eynde: Removed some unnecessary trailing commas in arrays
Kristiaan Van den Eynde: Moved all Token related code into a submodule
Kristiaan Van den Eynde: Adjusted all form functions to accept the form_state parameter to comply with the function signature in Drupal API
Kristiaan Van den Eynde: Completed functionality to mail views
Kristiaan Van den Eynde: Added a filter on the role submit form to filter out deselected checkboxes
Kristiaan Van den Eynde: Views submodule: Fixed a php notice and did an additional check in the target groups config page
Kristiaan Van den Eynde: Made ManyMail page tabs more consistent: always uses plural now
Kristiaan Van den Eynde: Started moving some code around for and wrote a large part of views submodule
Kristiaan Van den Eynde: Temporarily removed version dependencies in .info files until .x and .x-dev is supported
Kristiaan Van den Eynde: Removed a ton of whitespace errors
Kristiaan Van den Eynde: Added AJAX error handling and made the dialog's title translatable
Kristiaan Van den Eynde: Moved some code around in manymail_send_all_form to be consistent with its display
Kristiaan Van den Eynde: Updated dependency notation in info files
Kristiaan Van den Eynde: Fixed a bug where Drupal's Batch API would run multiple batches without respecting the configurable interval
Kristiaan Van den Eynde: Removed the need for adding the send mail batch delay through query string parameters, using Drupal.settings instead now
Kristiaan Van den Eynde: Updated 'real' ManyMail version of batch.js to reflect AJAX change in last commit
Kristiaan Van den Eynde: Added AJAX functionality and batch processing to the send form
Kristiaan Van den Eynde: Moved code for own version of batch.js from testing location to live location. Also added temporary file and disclaimers until Drupal Core issue #1320996 is fixed.
Kristiaan Van den Eynde: Added a different batch.js file
Kristiaan Van den Eynde: Put allowed roles options in fieldset for consistency
Kristiaan Van den Eynde: Corrected variable names of throttle options
Kristiaan Van den Eynde: Added options for throttling
Kristiaan Van den Eynde: Removed the need for manymail_targets_roles_form_validate()
Kristiaan Van den Eynde: Cleaned up batch redirection in _manymail_form_submit()
Kristiaan Van den Eynde: Fixed a bug in manymail_send_mail_batch() where parameters which are passed by reference (due to use of call_user_func_array()) were being manipulated
Kristiaan Van den Eynde: Unflagged 'allowed roles' as required on the administration page
Kristiaan Van den Eynde: Updated some doxygen to be more precise
Kristiaan Van den Eynde: Added an options page to the administration
Kristiaan Van den Eynde: Changed redirect of ManyMail batches to the ManyMail main page
Kristiaan Van den Eynde: Added mail all functionality and adjusted code to reuse common parts
Kristiaan Van den Eynde: Moved try-catch block to more logical place: manymail_send_mail()
Kristiaan Van den Eynde: Fixed a bug where PHPMailer would echo out errors instead of throwing Exceptions
Kristiaan Van den Eynde: Changed the mail functionality to use set_batch()
Kristiaan Van den Eynde: Adjusted manymail_send_role_form() to give a validation error for the Roles checkbox
Kristiaan Van den Eynde: Updated doxygen of manymail_send_mail() to specify a return value
Kristiaan Van den Eynde: Fixed a bug: trying to access the ErrorInfo property of the wrong object in manymail_send_mail()
Kristiaan Van den Eynde: Fixed wrong PHPMailer class filename in manymail_get_role_addresses()
Kristiaan Van den Eynde: Fixed semi-colons in ternary operators inmanymail_send_role_form_submit()
Kristiaan Van den Eynde: Updated some doxygen to be conform with the rest of the module
Kristiaan Van den Eynde: Updated description of role selection in manymail_send_role_form()
Kristiaan Van den Eynde: Adjusted manymail_get_role_addresses() to return recipient objects
Kristiaan Van den Eynde: Added manymail_send_role_form_submit()
Kristiaan Van den Eynde: Updated form structure in manymail_send_role_form()
Kristiaan Van den Eynde: More cleaning up of manymail_send_role_form()
Kristiaan Van den Eynde: Added a return value to manymail_send_mail()
Kristiaan Van den Eynde: Changed permissions to no longer require 'access manymail'
Kristiaan Van den Eynde: Fixed a bug where select-all JavaScript still used old project name
Kristiaan Van den Eynde: Adjusted doxygen for manymail_send_role_form() to be more clear
Kristiaan Van den Eynde: Merge branch 'master' of kristiaanvandeneynde@git.drupal.org:sandbox/kristiaanvandeneynde/1300826.git
Kristiaan Van den Eynde: Added a form for sending e-mail to user roles
Kristiaan Van den Eynde: Changed select-all behavior to use DRUPAL_AUTHENTICATED_RID
Kristiaan Van den Eynde: Cleaned up manymail_config_defaults_form
Kristiaan Van den Eynde: Added a check for administrator role
Kristiaan Van den Eynde: Changed file permissions to 644 and directory to 755
kristiaanvandeneynde: Changed the /manymail page contents.
kristiaanvandeneynde: Added a tab to the manymail page with custom access callbacks manymail_access_or() and manymail_access_and()
kristiaanvandeneynde: Added some content to the manymail path
kristiaanvandeneynde: Added a front-end page callback and a permission for it
kristiaanvandeneynde: First draft of manymail_send_mail() function
kristiaanvandeneynde: Removed testing code in manymail.module
kristiaanvandeneynde: Updated the message when PHPMailer is not found.
Kristiaan Van den Eynde: Initial commit with work-in-progress code
